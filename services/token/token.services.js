const jwt = require('jsonwebtoken')
const AppConf = require('../../config/app.config.json')

class TokenService {
  constructor() {
    this.data = new Map()
    this.authenticateToken = this.authenticateToken.bind(this)
  }

  singToken(user) {
    const token = jwt.sign({ user: user }, AppConf.tokenKey, { expiresIn: '1d' })
    this.data.set(token, token)
    return token
  }

  verifyToken(token) {
    const user = jwt.verify(token, AppConf.tokenKey)
    return user
  }

  removeToken(token) {
    this.data.delete(token)
  }

  async authenticateToken(req, res, next){
    const authHeader = req.headers.token
    if (authHeader) {
      const token = authHeader
      if (!this.data.has(token)) return res.sendStatus(403)
      const user = await this.verifyToken(token)
      req.user = user.user
      next()
      // jwt.verify(token, accessTokenSecret, (err, user) => {
      //   if (err) {
      //     console.log("err", err)
      //     return res.sendStatus(403)
      //   }
      //   req.user = user
      //   next()
      // })
    } else {
      res.sendStatus(401)
    }
  }
}


module.exports = new TokenService()
