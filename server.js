const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const cors = require("cors");
const { v4 } = require("uuid");
const socketIO = require("socket.io");
const MongoClient = require("mongodb").MongoClient;
const app = express();
const Login = require("./router/api/public/login/login");
const Appconf = require("./config/app.config.json");
const server = http.createServer(app);
const io = socketIO(server);

// MongoClient.connect('mongodb-connection-string', (err, client) => {
//     // ... do something here
//   })
// const uri = "mongodb+srv://nhuhuynh:Aa123123@cluster0.hyzyh.mongodb.net/test?retryWrites=true&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true,useUnifiedTopology: true });
// client.connect(err => {
//   const collection = client.db("test").collection("users");
//   // perform actions on the collection object
// //   app.get('/', function(req, res) {
// //     collection.find().toArray()
// //     res.json(collection.findOne())
// //   })

//   client.close();
// });
// var uri = "mongodb+srv://nhuhuynh:Aa123123@cluster0.hyzyh.mongodb.net";
MongoClient.connect(Appconf.database, { useUnifiedTopology: true }, function (
  err,
  db
) {
  if (err) throw err;
  console.log("connect success");
  const dbo = db.db("zalo");
  const users = dbo.collection("users");
  // socketIO

  //   dbo.createCollection("customers", function (err, res) {
  //     if (err) throw err;
  //     console.log("Collection created!");
  //     db.close();
  //   });
});

app.use(function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Credentials", true);
  // res.header("Access-Control-Allow-Headers", "X-Requested-With");
  // res.header("Access-Control-Allow-Headers", "Content-Type");
  // res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
//   next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// app.use(cors());
app.use("/api/public/login", Login);
io.on("connect", (socket) => {
  socket.on("logout", ({ username }) => {
    socket.broadcast.emit("logout", { username });
  });
  socket.on("enterMessage", ({ name, message }) => {
    socket.emit("enterMessage", { name, message });
  });
  //     socket.on("createRoom", ({ username }) => {

  //         dbo.createCollection
  //     });
});
app.listen(4000, function () {
  console.log("listening on 4000");
});
