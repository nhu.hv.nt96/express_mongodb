const express = require("express");
const { data } = require("../../../services/token/token.services");
const router = express.Router();

const rooms = [
  {
    id: "xx01",
    data: [
      { name: "lee", message: "Dawdawd" },
      { name: "riven", message: "Dawdawd" },
      { name: "lee", message: "Dawdawd" },
      { name: "riven", message: "Dawdawd" },
      { name: "lee", message: "Dawdawd" },
      { name: "lee", message: "Dawdawd" },
      { name: "riven", message: "Dawdawd" },

      { name: "lee", message: "Dawdawd" },

      { name: "lee", message: "Dawdawd" },
      { name: "rivent", message: "Dawdawd" },
      { name: "lee", message: "Dawdawd" },
      { name: "riven", message: "Dawdawd" },

      { name: "lee", message: "Dawdawd" },
      { name: "rivent", message: "Dawdawd" },
      { name: "lee", message: "Dawdawd" },
    ],
  },
  {
    id: "xx02",
    data: [
      { name: "lee", message: "sdfad" },
      { name: "vayne", message: "sdfnw3r" },
      { name: "lee", message: "msalkd" },
      { name: "vayne", message: "Dawdawd" },
      { name: "lee", message: "Dawdawd" },
      { name: "lee", message: "Dawdawd" },
      { name: "vayne", message: "Dawdawd" },

      { name: "lee", message: "Dawdawd" },

      { name: "lee", message: "Dawdawd" },
      { name: "vayne", message: "Dawdawd" },
      { name: "lee", message: "Dawdawd" },
      { name: "vayne", message: "Dawdawd" },

      { name: "lee", message: "sdfjqn" },
      { name: "vayne", message: "msanmjkeha n" },
      { name: "lee", message: "msdfnnaa" },
    ],
  },
  {
    id: "xx03",
    data: [
      { name: "lee", message: "Dawdawd" },
      { name: "darius", message: "Dawdawd" },
      { name: "lee", message: "Dawdawd" },
      { name: "darius", message: "Dawdawd" },
      { name: "lee", message: "Dawdawd" },
      { name: "lee", message: "Dawdawd" },
      { name: "darius", message: "Dawdawd" },

      { name: "lee", message: "Dawdawd" },

      { name: "lee", message: "Dawdawd" },
      { name: "darius", message: "Dawdawd" },
      { name: "lee", message: "lqwfh" },
      { name: "darius", message: "jknaslkd" },

      { name: "lee", message: "njshal" },
      { name: "darius", message: ",anvbah" },
      { name: "lee", message: "yen,a" },
    ],
  },
];

router.get("/", (req, res) => {
  const data = rooms.find((val) => val.id === req.body.room);
  return res.json({
    code: "MESSAGE_2000",
    data: rooms,
    message: "list message",
  });
});

router.post("/", (req, res) => {
  const data = rooms.find((val) => val.id === req.body.room);
  if (data) {
    return res.json({
      code: "ROOM_2001",
      data,
      message: "data room",
    });
  }
  return res.json({
    code: "ROOM_4001",
    data: {},
    message: "khon tim thay",
  });
});

module.exports = router;
