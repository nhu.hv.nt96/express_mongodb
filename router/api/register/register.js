const express = require('express')
const User = require('../../../models/user/user')
const router = express.Router()

router.post('/', (req, res) => {
  const { fullname, username, password, phoneNumber } = req.body
  User.findOne({ username: username }, (err, user) => {
    if (err) {
      return
    }

    if (user) {
      return res.json({
        code: 'REGISTER_4000',
        data: {},
        message: "username đã tồn tại"
      })
    }

    const newUser = new User({
      fullname: fullname,
      phoneNumber: phoneNumber,
      username: username,
      password: password
    })

    newUser.save()
      .then(data => {
        res.json({
          code: 'REGISTER_2000',
          data: {},
          message: "register thanh cong"
        })
      })
      .catch(err => {
        res.json({ message: err })
      })
  })
})

module.exports = router