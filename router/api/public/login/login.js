const express = require("express");
const MongoClient = require("mongodb").MongoClient;
const router = express.Router();
const Appconf = require("../../../../config/app.config.json");
// const User = require('../../../models/user/user')
const tokenService = require("../../../../services/token/token.services");

router.post("/", async (req, res) => {
  const { phoneNumber, password } = req.body;
  MongoClient.connect(
    Appconf.database,
    { useUnifiedTopology: true },
    (err, db) => {
      const dbo = db.db("zalo");
      const collection = dbo.collection("users");
    
      collection.findOne(
        { phoneNumber: phoneNumber, password: password },
        (err, user) => {
          if (err) {
            return res.json({ err });
          }
          if (!user) {
            return res.json({
              code: "LOGIN_4000",
              data: {},
              message: "login failed",
            });
          }
          const token = tokenService.singToken(user);
          res.json({
            code: "LOGIN_2000",
            message: "login success",
            data: {
              token,
              phoneNumber: user.phoneNumber,
            },
          });
        }
      );
    }
  );
});

module.exports = router;
