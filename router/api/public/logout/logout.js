const express = require('express')
const tokenService = require('../../../services/token/token.services')
const router = express.Router()

router.post('/', (req, res) => {
  const { token } = req.body
  tokenService.removeToken(token)
  res.json({
    code: "LOGOUT_2000",
    data: {},
    message: "login success"
  })
})

module.exports = router