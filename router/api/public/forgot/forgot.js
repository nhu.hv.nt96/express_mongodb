const express = require('express')
const router = express.Router()
const User = require('../../../../models/user/user')
const tokenService = require('../../../../services/token/token.services')

router.post('/', async (req, res) => {
  const { username, fullname } = req.body
  User.findOne({ username: username, fullname: fullname }, (err, user) => {
    if (err) {
      return res.json({ err })
    }
    if (!user) {
      return res.json({
        code: "FORGOT_4000",
        data: {},
        message: "Thông tin không hợp lệ"
      })
    }
    const token = tokenService.singToken(user)
    return res.send({
      code: "FORGOT_2000",
      data: {
        token
      },
      message: "thong tin hop le"
    })
  })
})

router.put('/', tokenService.authenticateToken, (req, res) => {
  const { password } = req.body
  User.findById(req.user._id, (err, user) => {
    if (err) {
      return res.json({
        code: "FORGOT_4001",
        data: {},
        message: "thay doi that bai"
      })
    }
    user.password = password
    user.save()
    res.json({
      code: "FORGOT_2001",
      data: {},
      message: "thay doi mat khau thanh cong"
    })
  })
})

module.exports = router